﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballMovement : MonoBehaviour {
    
    Rigidbody rb;
    Vector3 initMovement;

    public float speed;

    public GameObject bounceHintObj;
    private GameObject hint;
    RaycastHit hit;
    LineRenderer line;
    //timer for random movement
    public float StartTime = 0;
    public breakGM gmref;
    public bool useStartTime = false;
    public bool randomStartSpeed = false;
    public float setZspeed = 10;
    //
    public int playerNum = 1;
    //
    public GameObject partSysObj;
    // Use this for initialization
    void OnEnable()
    {

        rb = GetComponent<Rigidbody>();
        gmref = GameObject.FindGameObjectWithTag("gm").GetComponent<breakGM>();
        StartTime = gmref.StartWaittime;
        hint = Instantiate(bounceHintObj, transform.position, transform.rotation) as GameObject;

        if (useStartTime)
            StartCoroutine(StartBallmovement());

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Drawing the BounceHint Object
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        //Debug.DrawRay(transform.position, fwd * 50, Color.green);
        if (Physics.Raycast(transform.position, fwd, out hit, 100))
        {
            hint.transform.position = hit.point;
            hint.transform.rotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
        }
        transform.LookAt(transform.position + rb.velocity);
    }

    public IEnumerator StartBallmovement()
    {
        yield return new WaitForSeconds(StartTime);
        InitMovement();
    }

    public void InitMovement()
    {
        float x = Random.Range(-5, 5);
        float y = Random.Range(-5, 5);
        float z = Random.Range(-10, 10);

        if (randomStartSpeed)
        {
            initMovement = new Vector3(x, y, z);
        }
        else
        {
            float mul = Random.Range(-1, 1);
            if (mul < 0)
            {
                initMovement = new Vector3(x, y, setZspeed * -1);
            }
            else
            {
                initMovement = new Vector3(x, y, setZspeed);
            }
        }
        rb.velocity = initMovement * speed * Time.fixedDeltaTime;
    }

    public void Hitmovement(Vector3 newdir)
    {
        newdir.z = setZspeed;
        rb.velocity = newdir * speed * Time.fixedDeltaTime;
    }

    public void OnCollisionEnter(Collision col)
    {
        GameObject pos = Instantiate(partSysObj, col.contacts[0].point, Quaternion.identity) as GameObject;

    }


}
