﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class breakGM : MonoBehaviour {
 
    public Transform ballStartloc1;
    public Transform ballStartloc2;
    public GameObject ballObj;
 
    public GameObject pongBallPlayer1;
    public GameObject pongBallPlayer2;

    public float StartWaittime = 3;
    public bool newgame = false;
 
    public void Start()
    {
        NextGame();
    }


    public void NextGame()
    {
            newgame = true;
            pongBallPlayer1 = Instantiate(ballObj, ballStartloc1.position, ballStartloc1.rotation) as GameObject;        
            pongBallPlayer2 = Instantiate(ballObj, ballStartloc2.position, ballStartloc2.rotation) as GameObject;
        print("creating balls");
        ballMovement bmove = pongBallPlayer1.GetComponent<ballMovement>();
        bmove.playerNum = 1;
        bmove = pongBallPlayer2.GetComponent<ballMovement>();
        bmove.playerNum = 2;

        StartCoroutine(goAgain());
    }

    IEnumerator goAgain()
    {
        yield return new WaitForSeconds(.3f);
        newgame = false;
    }

    public void destroyBalls()
    {
        Destroy(pongBallPlayer1.gameObject);
        Destroy(pongBallPlayer2.gameObject);
    }
}
