﻿using UnityEngine;
using System.Collections;

public class goalAction : MonoBehaviour {

    public ScoreManagement scoremanref;

    public int playerNum = 1;

    public void OnCollisionEnter(Collision col)
    {
        if (col.transform.CompareTag("Pong"))
        {
            print("hit");
            if (playerNum == 1)
            {
                scoremanref.PlayerOneScore();
            }
            else
            {
                scoremanref.PlayerTwoScore();
            }
        }
        gm.manager.newgame = true;
        
    }

}
