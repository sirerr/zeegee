﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PongBounce : MonoBehaviour {

    Rigidbody rb;
    Vector3 initMovement;

    public float speed;
    public float minSpeed;
    public float dragAmount;

    public GameObject bounceHintObj;
    private GameObject HintObj;
    RaycastHit hit;
    LineRenderer line;
    //timer for random movement
    float StartTime = 0;
    gm gmref;
    public bool useStartTime = true;
    public bool randomStartSpeed = false;
    public float setZspeed = 10;
    //
    public int playerNum = 1;

    //
    public GameObject partSysObj;
    // Use this for initialization

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        gmref = gm.manager;
        StartTime = gmref.StartWaittime;
        HintObj = gm.manager.HinterObj;
    }


   public void LetBegin () {
        rb.velocity = Vector3.zero;
        if (useStartTime)
            StartCoroutine(StartBallmovement());
    }
	
	// Update is called once per frame
	void FixedUpdate () {

 
        //Drawing the BounceHint Object
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        //Debug.DrawRay(transform.position, fwd * 50, Color.green);
        if (Physics.Raycast(transform.position, fwd, out hit, 100))
        {
            HintObj.transform.position = hit.point;
            HintObj.transform.rotation = Quaternion.FromToRotation(Vector3.forward,hit.normal);
        }
        transform.LookAt(transform.position + rb.velocity);
        
 
    }

    //void DrawForwardLine()
    //{
    //    Vector3 posOne = transform.position;
    //    Vector3 posTwo = transform.forward * 10;
    //    line.SetPosition(0, posOne);
    //    line.SetPosition(1, posTwo);
    //}

    public  IEnumerator StartBallmovement()
    {
      //  print(StartTime);
        yield return new WaitForSeconds(StartTime);
        InitMovement();
    }

   public void InitMovement()
    {
        float x = Random.Range(-5, 5);
        float y = Random.Range(-5, 5);
        float z = Random.Range(-10, 10);

        if (randomStartSpeed)
        {
            initMovement = new Vector3(x, y, z);
        }
        else
        {
            float mul = Random.Range(-1,1);
            if (mul < 0)
            {
                initMovement = new Vector3(x, y, setZspeed * -1);
            }
            else
            {
                initMovement = new Vector3(x, y, setZspeed);
            }
        }
        rb.velocity = initMovement * speed * Time.fixedDeltaTime;
    }

    public void Hitmovement(Vector3 newdir)
    {
        newdir.z = setZspeed;
        rb.velocity = newdir * speed * Time.fixedDeltaTime;
    }

    public void OnCollisionEnter(Collision col)
    {
        GameObject pos = Instantiate(partSysObj, col.contacts[0].point, Quaternion.identity) as GameObject;

        if (col.gameObject.CompareTag("goal"))
        {
           //  Destroy(hint);
        }
    }


}
