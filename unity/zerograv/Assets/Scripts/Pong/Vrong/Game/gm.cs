﻿using UnityEngine;
using System.Collections;

public class gm : MonoBehaviour {

    public Transform ballStartloc0;
    public Transform ballStartloc1;
    public Transform ballStartloc2;
    public GameObject ballObj;
    public GameObject MainBall;

    public GameObject pongBallPlayer1;
    public GameObject pongBallPlayer2;

    public GameObject HinterObj;
    public float StartWaittime =3;
    public bool newgame = false;
    public static gm manager;
    public bool brickBreakMode = false;
    bool FirstGame = true;
    public void Start()
    {
        manager = this;
        NewNormalGame();
    }


    public void NextGame()
    {
        if (brickBreakMode)
        {
            newgame = true;
            pongBallPlayer1 = Instantiate(ballObj, ballStartloc1.position, ballStartloc1.rotation) as GameObject;
            pongBallPlayer2 = Instantiate(ballObj, ballStartloc2.position, ballStartloc2.rotation)as GameObject;
        }
        else
        {
            newgame = true;
            MainBall = Instantiate(ballObj, ballStartloc0.position, ballStartloc0.rotation) as GameObject;
        } 
        StartCoroutine(goAgain());
    }

    IEnumerator goAgain()
    {
        yield return new WaitForSeconds(.3f);
        newgame = false;
        
    }

    public void NewNormalGame()
    {
        if(FirstGame)
            MainBall = Instantiate(ballObj, ballStartloc0.position, ballStartloc0.rotation) as GameObject;
        FirstGame = false;
        MainBall.transform.position = ballStartloc0.position;
        MainBall.GetComponent<PongBounce>().LetBegin();
    }
}
