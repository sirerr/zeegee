﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManagement : MonoBehaviour {

    public Text p1text;
    public Text p2text;

    public int Highscore = 13;
    public int p1Score =0;
    public int p2Score =0;

    public gm gmref;
  

   	// Use this for initialization
	void Start () {
        gmref = GetComponent<gm>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Gamereset()
    {
        p1Score = 0;
        p2Score = 0;

        Scene currentscene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentscene.name);
    }


    public void PlayerOneScore()
    {
        p1Score++;

        p1text.text = p1Score.ToString();
        if (p1Score == Highscore)
        {
            Gamereset();
        }
        else
        {
            gmref.NewNormalGame();
        }


    }

    public void PlayerTwoScore()
    {
        p2Score++;

        p2text.text = p2Score.ToString();
        if (p2Score == Highscore)
        {
            Gamereset();
        }
        else
        {
            gmref.NewNormalGame();
        }
    }
}
