﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particleEffect : MonoBehaviour {

    private ParticleSystem partSys;

    public void Awake()
    {
        partSys = GetComponent<ParticleSystem>();
    }

	// Update is called once per frame
	void Update () {

        if (!partSys.IsAlive())
        {
            Destroy(gameObject);
        }
	}
}
