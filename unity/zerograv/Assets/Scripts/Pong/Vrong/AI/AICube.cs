﻿using UnityEngine;
using System.Collections;

public class AICube : MonoBehaviour {

    public GameObject pongBall;
    public gm gmref;

    private float imperfectTimer = 0;
    public float imperfectTimerlimit =3;

    private float imperfectvalue = 0;
    public float imperfectValueRequired = .6f;

    public float imperfectChangeValue = 3;
    private int hitCount = 0;
    public int hitCountLimit = 3;

    public float speed = 1;
    public float extraTime = 10;

    public GameObject indic;
	// Use this for initialization
	void Start () {
        pongBall = gmref.MainBall;
    }
	
	// Update is called once per frame
	void Update () {
        if (pongBall == null)
        {
            pongBall = gmref.pongBallPlayer2;
        }

        if (gmref.newgame)
        {
            imperfectTimer = 0;
            imperfectvalue = Random.value;
            gmref.newgame = false;
        }

        imperfectTimer += Time.deltaTime;


        if ((imperfectTimer > imperfectTimerlimit && imperfectvalue>imperfectValueRequired)&& pongBall!=null)
        {
            //simple transform code
            //Vector3 pongBallPos = pongBall.transform.position;
            //gameObject.transform.position = new Vector3(pongBallPos.x -imperfectChangeValue, pongBallPos.y-imperfectChangeValue, transform.position.z);
        
            //lerp code
            Vector3 pongBallPos = new Vector3(pongBall.transform.position.x, pongBall.transform.position.y, transform.position.z);
            float newSpeed = (speed / extraTime) * Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, pongBallPos,newSpeed);
            // lerp code

            indic.SetActive(true);


        }
        else if(pongBall!=null)
        {
            //simple transform code
            Vector3 pongBallPos = pongBall.transform.position;
            gameObject.transform.position = new Vector3(pongBallPos.x, pongBallPos.y, transform.position.z);
            indic.SetActive(false);
        }


    }

    public void OnCollisionEnter(Collision col)
    {
        hitCount++;

        if (hitCount > hitCountLimit)
        {
            hitCount = 0;
            imperfectvalue = Random.value;
        }
    }

}
