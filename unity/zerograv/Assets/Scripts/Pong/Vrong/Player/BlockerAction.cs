﻿using UnityEngine;
using System.Collections;

public class BlockerAction : MonoBehaviour {

    public float controllerDistanceReq = 10;
    public float controllerDistanceRealTime = 0;

    public Transform wand1;
    public Transform wand2;

    public GameObject FrontBlocker;


    void FixedUpdate()
    {

    }

    public void OpenPaddle()
    {
        controllerDistanceRealTime = Vector3.Distance(wand1.position, wand2.position);

        /*
        if (controllerDistanceRealTime < controllerDistanceReq)
        {
            FrontBlocker.SetActive(true);
        }
        else
        {
            FrontBlocker.SetActive(false);
        }
        */

    }

    public void OnCollisionEnter(Collision col)
    {
        if (col.transform.CompareTag("Pong"))
        {

        }
    }

}
