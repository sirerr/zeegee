﻿using UnityEngine;
using System.Collections;

public class PaddleGazePosition : MonoBehaviour {

    [SerializeField]
    GameObject gazePlane;

    [SerializeField]
    GameObject debugObject;

    [SerializeField]
    GameObject mainCamera;

    public float speed;
    public float xDist;
    public float yDist;

    Vector3 oriPos;

    Ray ray;
    public float maxDistance = 4;
    private Plane planeObj;

	// Use this for initialization
	void Start () {
        oriPos = transform.position;

        planeObj = new Plane(transform.forward, transform.position);
	}
	
	// Update is called once per frame
	void Update () {

        RaycastHit hit;
        float hitdistance;

        Vector3 fwd = mainCamera.transform.TransformDirection(Vector3.forward);
        Vector3 currentPos = transform.position;

        //Draw Ray from Camera
        Debug.DrawRay(mainCamera.transform.position, fwd, Color.green);

        //  if (Physics.Raycast(mainCamera.transform.position, fwd, out hit))
        if (planeObj.Raycast(new Ray(mainCamera.transform.position, fwd), out hitdistance))
        {
            //if(hit.transform.gameObject == gazePlane)
            //{
            //    //debugObject.transform.position = hit.point;

            //}

            Vector3 hitPos = mainCamera.transform.position + fwd * hitdistance;
          //  Vector3 hitPos = new Vector3(hit.point.x, hit.point.y, oriPos.z);
            //float x = Mathf.Clamp(transform.position.x, oriPos.x + (xDist * -1), oriPos.x + xDist);
            //float y = Mathf.Clamp(transform.position.y, oriPos.y + (yDist * -1), oriPos.y + yDist);

            //previous working
            Vector3 moveTo = new Vector3(
               Mathf.Clamp(hitPos.x, hitPos.x + (xDist * -1), hitPos.x + xDist), 
               Mathf.Clamp(hitPos.y, hitPos.y + (yDist * -1), hitPos.y + yDist), oriPos.z);


            /*
            Vector3 moveTo = new Vector3(Mathf.Clamp(currentPos.x, hit.point.x + (xDist*-1), hit.point.x + xDist),
                Mathf.Clamp(currentPos.y, hit.point.y + (yDist * -1), hit.point.y + yDist), oriPos.z);

                    transform.position = Vector3.Lerp(transform.position, new Vector3(
                Mathf.Clamp(oriPos.x, hitPos.x + (xDist * -1), hitPos.x + yDist), 
                Mathf.Clamp(oriPos.y, hitPos.y + (yDist * -1), hitPos.y + yDist), hitPos.z),
                Time.deltaTime * speed);
                */
            Vector3 camPos = mainCamera.transform.position;
            camPos.z = oriPos.z;
            float dis = Vector3.Distance(camPos, moveTo);

            if (dis > maxDistance)
            {
                moveTo = camPos + (moveTo - camPos).normalized * maxDistance;
            }
            transform.position = Vector3.Lerp(transform.position, moveTo, Time.deltaTime * speed);


            //   Valve.VR.HmdQuad_t bounds = new Valve.VR.HmdQuad_t();
            //   SteamVR_PlayArea.GetBounds(SteamVR_PlayArea.Size.Calibrated, ref bounds);

        }
    }
}
